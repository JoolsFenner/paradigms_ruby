def fave_band(options = {})
	case options[:band]
	when :beatles
		'hooray'
	when :nirvana
		'boo'
	else
		'huh?'
	end
end

puts fave_band()
puts fave_band(:band => :nirvana)