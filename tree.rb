class Tree
	#Mixin example, see each and <=> below	
	
	include Enumerable
	attr_accessor :children, :node_name
	
	def initialize(name, children=[])
		@children = children
		@node_name = name
	end

#this is redundant because of "each" being included.
	def traverse(&block)
		process &block
		children.each {|c| c.traverse &block}
	end
	
	def process(&block)
		block.call self
	end

=begin
#implemented for Enumberable so its method sort can be used
#http://ruby-doc.org/core-2.1.3/Enumerable.html
	def each(&block)
		block.call self
		children.each {|c| c.each &block}
	end
# Enumberable sort also needs spaceship operator.  Used for comparing two objects
	def <=>(t)
		return -1 if self.node_name < t.node_name
		return 1 if self.node_name > t.node_name
		return 0 if self.node_name == t.node_name
		return nil
	end
=end	
end


rubytree = Tree.new('Ruby', [Tree.new('Reia'), Tree.new('MacRuby')])

puts 'Processing the root node by outputting its name'
rubytree.process {|node| puts node.node_name}
puts''

puts 'Traversing the whole tree (printing each node name)'
rubytree.traverse {|node| puts node.node_name}
puts''

=begin
puts 'Traversing the whole tree using Enumberable sort (mixin)'
rubytree.sort
rubytree.traverse {|node| puts node.node_name}
puts''

puts "Sorting tree using array's each method"
(rubytree.sort).each {|n| puts n.node_name}
=end



#inheritance, creating red-black tree.
class RedBlackTree < Tree
	attr_accessor :color
	
	def initialize(name, color, children=[])
		super(name, children)
		@color = color
	end
	
	def balance()
	#...
	end

end
