#refactored Tree from tree.rb
#traverse no longer needed

class Tree
	include Enumerable
	attr_accessor :children, :node_name
	
	def initialize(name, children=[])
	@children = children
	@node_name = name
	end
	
	def each(&block)
	block.call self
	children.each {|c| c.each &block}
	end
	
	def <=>(t)
		return -1 if self.node_name < t.node_name
		return 1 if self.node_name > t.node_name
		return 0 if self.node_name == t.node_name
		return nil
	end
	
end

rubytree = Tree.new('Ruby', [Tree.new('Reia'), Tree.new('MacRuby')])
(rubytree.sort).each {|n| puts n.node_name}