books = {}
books ["The Trial"] = :oustanding
books ["Gravity's Rainbow"] = :great
books ["Crime & Punishment"] = :great
books ["High Fidelity"] = :rubbish
books ["Don Quizote"] = :quiteGood

ratings = Hash.new(0)
books.values.each { |rate| ratings[rate] += 1 }


puts 'PRINT ALL KEYS'
puts books.keys
puts''

puts 'PRINT ALL VALUES'
puts books.values
puts''

puts 'COUNT ALL RATINGS'
puts ratings
puts''