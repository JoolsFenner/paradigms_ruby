=begin
Ruby will invoke the included method whenever this module gets included
into another. Remember, a class is a module. In our included
method, we extend the target class called base (which is the RubyCsv
class), and that module adds class methods to RubyCsv. The only class
method is acts_as_csv. That method in turn opens up the class and
includes all the instance methods. And we�re writing a program that
writes a program.
=end


module ActsAsCsv
	def self.included(base)
		base.extend ClassMethods
	end
	
	module ClassMethods
		def acts_as_csv
			include InstanceMethods
		end
	end
	
	module InstanceMethods
	
		def read
			@csv_contents = []
			filename = self.class.to_s.downcase + '.txt'
			file = File.new(filename)
			@headers = file.gets.chomp.split(', ' )
		
			file.each do |row|
				@csv_contents << row.chomp.split(', ' )
			end
		end
	
		attr_accessor :headers, :csv_contents
	
		def initialize
			read
		end
	
	end
	
end
	
class RubyCsv # no inheritance! You can mix it in
	include ActsAsCsv
	acts_as_csv
end

m = RubyCsv.new
puts m.headers.inspect
puts m.csv_contents.inspect