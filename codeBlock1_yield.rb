class Fixnum
	def log2times
		i = self
		while i > 1
			i = i / 2
			yield
		end
	end
end


50000.log2times {puts "hello world"}