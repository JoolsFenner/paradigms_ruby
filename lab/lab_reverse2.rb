class MyList

	def initialize(list=[])
		@list = list
	end
	
	def my_method(&block)
		@list.each {|a| yield a}
		call block
	end
	
end

jools = MyList.new([1,2,3])

jools.my_method {|a| a.reverse.each}