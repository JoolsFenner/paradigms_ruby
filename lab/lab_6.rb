=begin
Rewrite the class Bottle below so that an object of type Bottle does not need to
check its content when serving it. Hint: create classes for the content and during the
initialisation of a Bottle object, it receives a Content object. When serving the content,
the Bottle object will delegate the job to the Content object.
=end

class Bottle
	attr_accessor :content
	
	def initialize(content)
		@content = content
	end
	
	def serve
		if content.liquid == "water"
			puts "pour #{content.liquid}"
		elsif content.liquid == "wine"
			puts "decant #{content.liquid}"
		else
			puts "no idea what this is"
		end
	end
	
	def test
		if content.liquid == "wine"
			puts "ITS WINE"
		end	
	end
end

class Content
	attr_accessor :liquid
	
	def initialize(liquid)
		@liquid = liquid
	end
	
end

bottle1 = Bottle.new(Content.new("wine"))
bottle1.serve


