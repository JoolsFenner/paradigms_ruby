#method 1
def print_file(path)
	i = 1
	File.foreach(path) do |line|
		puts i.to_s + ": " + line
		i = i + 1
	end
end

#method 2
def print_file2(path)
	f = File.new(path)
	f.each_with_index do |n, i|
		puts i.to_s + ": " + n
	end
end	


#method 3
def print_file3(path)
	f = File.new(path)
	while line = f.gets
		print $.
		puts ": " + line
	end
	f.close
end

#method 4 TO DO
def print_file4(&block)


end


puts("using print_file(path)")
print_file("lab_4.rb")


puts()
puts()
puts("using each_with_index")
print_file2("lab_4.rb")



puts()
puts()
puts("using libary methods")
print_file3("lab_4.rb")


=begin 
puts()
puts()
puts("using code blocks")
f = File.new("lab_4.rb")
=end






