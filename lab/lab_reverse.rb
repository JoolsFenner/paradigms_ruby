class MyList
	attr_reader :list 
	def initialize(list=[])
		@list = list
	end
	
	def meth(&block) 
		self.instance_eval &block
	end
	
end


jools = MyList.new([1,2,3])
puts jools.list
p ""

puts jools.meth {@list.reverse}