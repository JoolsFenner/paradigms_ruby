=begin
rewrite this program in an object-oriented programming style. For an additional chal-
lenge, do so without using any if, unless, case statements or other conditionals.

def doX(osname)
	if osname == :Windows
		puts "doXWindows"
	elsif osname == :Linux
		puts "doXLinux"
	elsif osname == :Mac
		puts "doXMac"
	else
		puts "unknown OS"
	end
end

doX(:Windows)
=end


#TO DO (WRONG)
class OS
	def doX(osname)
		puts osname
	end
end

class Windows < OS
	attr_accessor :osname
	
	def initialize(osname)
		@osname = osname
	end

end

windows = Windows.new("windows")
