class TestClass
	#allows access to sqrt
	include Math
	
	def initialize(x)
		@x = x
	end
	
	def what_is_sq_root
		puts @x
		puts sqrt(@x)
	end
end

x = TestClass.new(8)
x.what_is_sq_root