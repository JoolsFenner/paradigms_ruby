class Person	
	
	attr_accessor :name, :age, :city, :faveMusic, :time
	
	def initialize(name, age, city, faveMusic)
		@name = name
		@age = age
		@city = city
		@faveMusic = faveMusic
		@time = Time.now
	end
end



jools = Person.new("Julian", 33, "London", "Techno")
linda = Person.new("Linda", 32, "Motherwell", "Indie")
andrea = Person.new("Andrea", 36, "London", "Electro")
martin = Person.new("Pablo", 30, "Pablo", "Classical")


people = [jools, linda, andrea, martin]

#print people.sort_by { |x| x.time}.reverse
#print people.find_all { |x| x.fulltext.match(/London/i)}
#people << <new_entry>
print people.map { |x| x.city}


=begin
jools.name  = "Julian"
jools.age  = 33
jools.city = "london"
jools.faveMusic = "techno"
=end