=begin
special debugging method each time a method is missing
in order to print some diagnostic information. This behavior makes
the language easier to debug. But sometimes, you can take advantage
of this language feature to build some unexpectedly rich behavior. All
you need to do is override method_missing. Consider an API to represent
Roman numerals. You could do it easily enough with a method call,
with an API something like Roman.number_for "ii". In truth, that�s not too
bad. There are no mandatory parentheses or semicolons to get in the
way

=end

class Roman
	
	def self.method_missing name, *args
	# method_missing overridden.  The name of the method and its parameters are input parameters. Only using name here
	roman = name.to_s
		roman.gsub!("IV" , "IIII" )
		roman.gsub!("IX" , "VIIII" )
		roman.gsub!("XL" , "XXXX" )
		roman.gsub!("XC" , "LXXXX" )
		
		(roman.count("I" ) +
		roman.count("V" ) * 5 +
		roman.count("X" ) * 10 +
		roman.count("L" ) * 50 +
		roman.count("C" ) * 100)
	end
end

puts Roman.CCCC
puts Roman.XC
puts Roman.XII
puts Roman.X
