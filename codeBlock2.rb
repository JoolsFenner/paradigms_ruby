class Array
  def iterate!
    self.each_with_index do |n, i|
      self[i] = yield(n)
    end
  end
end

array = [1, 2, 3, 4]

array.iterate! do |n|
  n ** 2
end

puts array.inspect

=begin
http://www.reactive.io/tips/2008/12/21/understanding-ruby-blocks-procs-and-lambdas/
To start off, we re-opened the Array class and put our iterate! method inside. We will keep with Ruby conventions and put a bang at the end, letting our users know to watch out, as this method might be dangerous! We then use our iterate! method just like Ruby�s built in collect! method. The neat stuff however, is right in the middle of our iterate! method definition.

Unlike attributes, you do not need to specify the name of blocks within your methods. Instead, you can use the yield keyword. Calling this keyword will execute the code within the block provided to the method. Also, notice how we are passing n (the integer that the each_with_index method is currently working with) to yield. The attributes passed to yield corresponds to the variable specified in the piped list of the block. That value is now available to the block and returned by the yield call. So to recap what is happening:

    Send iterate! to the Array of numbers.
    When yield is called with the number n (first time is 1, second time is 2, etc�), pass the number to the block of code given.
    The block has the number available (also called n) and squares it. As it is the last value handled by the block, it is returned automatically.
    Yield outputs the value returned by the block, and rewrites the value in the array.
    This continues for each element in the array.
=end